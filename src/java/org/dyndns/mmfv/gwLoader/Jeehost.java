/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dyndns.mmfv.gwLoader;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author verranm
 */
@Entity
@Table(name = "jeehost")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Jeehost.findAll", query = "SELECT j FROM Jeehost j"),
    @NamedQuery(name = "Jeehost.findByHostId", query = "SELECT j FROM Jeehost j WHERE j.hostId = :hostId"),
    @NamedQuery(name = "Jeehost.findByHostName", query = "SELECT j FROM Jeehost j WHERE j.hostName = :hostName"),
    @NamedQuery(name = "Jeehost.findByHosttype", query = "SELECT j FROM Jeehost j WHERE j.hosttype = :hosttype")})
public class Jeehost implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "host_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long hostId;
    @Basic(optional = false)
    @Column(name = "host_name", nullable = false, length = 255)
    private String hostName;
    @Column(name = "hosttype", length = 255)
    private String hosttype;
    @OneToMany(mappedBy = "hostId")
    private List<Jeecell> jeecellList;

    public Jeehost() {
    }

    public Jeehost(Long hostId) {
        this.hostId = hostId;
    }

    public Jeehost(Long hostId, String hostName) {
        this.hostId = hostId;
        this.hostName = hostName;
    }

    public Long getHostId() {
        return hostId;
    }

    public void setHostId(Long hostId) {
        this.hostId = hostId;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getHosttype() {
        return hosttype;
    }

    public void setHosttype(String hosttype) {
        this.hosttype = hosttype;
    }

    @XmlTransient
    public List<Jeecell> getJeecellList() {
        return jeecellList;
    }

    public void setJeecellList(List<Jeecell> jeecellList) {
        this.jeecellList = jeecellList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (hostId != null ? hostId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jeehost)) {
            return false;
        }
        Jeehost other = (Jeehost) object;
        //if ((this.hostId == null && other.hostId != null) || (this.hostId != null && !this.hostId.equals(other.hostId))) {
        //    return false;
        //}
        return true;
    }

    @Override
    public String toString() {
        return "gwloader.Jeehost[ hostId=" + hostId + " ]";
    }
    
}
