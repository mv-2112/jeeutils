/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dyndns.mmfv.gwLoader;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author verranm
 */
public class utils {

    public static Date stringToDate(String stringDate) {
        // format is 20140807T172730
        Date convertedDate = null;
        if (stringDate.length() == 15) {
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
            try {
                convertedDate = dateFormat.parse(stringDate);
            } catch (ParseException ex) {
                convertedDate = null;
            }
        }
        return convertedDate;
    }

}
