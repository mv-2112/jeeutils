/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dyndns.mmfv.gwLoader;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author verranm
 */
@Entity
@Table(name = "jeecell")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Jeecell.findAll", query = "SELECT j FROM Jeecell j"),
    @NamedQuery(name = "Jeecell.findByCellId", query = "SELECT j FROM Jeecell j WHERE j.cellId = :cellId"),
    @NamedQuery(name = "Jeecell.findByCellName", query = "SELECT j FROM Jeecell j WHERE j.cellName = :cellName"),
    @NamedQuery(name = "Jeecell.findByCellNameAndHostId", query = "SELECT j FROM Jeecell j WHERE j.cellName = :cellName AND j.hostId = :hostId")})

public class Jeecell implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "cell_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long cellId;
    @Basic(optional = false)
    @Column(name = "cell_name", nullable = false, length = 255)
    private String cellName;
    @JoinColumn(name = "host_id", referencedColumnName = "host_id")
    @ManyToOne
    private Jeehost hostId;
    @OneToMany(mappedBy = "cellId")
    private List<Jeecriteria> jeecriteriaList;

    public Jeecell() {
    }

    public Jeecell(Long cellId) {
        this.cellId = cellId;
    }

    public Jeecell(Long cellId, String cellName) {
        this.cellId = cellId;
        this.cellName = cellName;
    }

    public Long getCellId() {
        return cellId;
    }

    public void setCellId(Long cellId) {
        this.cellId = cellId;
    }

    public String getCellName() {
        return cellName;
    }

    public void setCellName(String cellName) {
        this.cellName = cellName;
    }

    public Jeehost getHostId() {
        return hostId;
    }

    public void setHostId(Jeehost hostId) {
        this.hostId = hostId;
    }

    @XmlTransient
    public List<Jeecriteria> getJeecriteriaList() {
        return jeecriteriaList;
    }

    public void setJeecriteriaList(List<Jeecriteria> jeecriteriaList) {
        this.jeecriteriaList = jeecriteriaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cellId != null ? cellId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jeecell)) {
            return false;
        }
        Jeecell other = (Jeecell) object;
        if ((this.cellId == null && other.cellId != null) || (this.cellId != null && !this.cellId.equals(other.cellId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gwloader.Jeecell[ cellId=" + cellId + " ]";
    }
    
}
