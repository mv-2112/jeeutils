/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dyndns.mmfv.gwLoader;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author verranm
 */
@Entity
@Table(name = "jeecriteria")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Jeecriteria.findAll", query = "SELECT j FROM Jeecriteria j"),
    @NamedQuery(name = "Jeecriteria.findByCriteriaId", query = "SELECT j FROM Jeecriteria j WHERE j.criteriaId = :criteriaId"),
    @NamedQuery(name = "Jeecriteria.findByActualresult", query = "SELECT j FROM Jeecriteria j WHERE j.actualresult = :actualresult"),
    @NamedQuery(name = "Jeecriteria.findByCriteriaName", query = "SELECT j FROM Jeecriteria j WHERE j.criteriaName = :criteriaName"),
    @NamedQuery(name = "Jeecriteria.findByExpectedresult", query = "SELECT j FROM Jeecriteria j WHERE j.expectedresult = :expectedresult"),
    @NamedQuery(name = "Jeecriteria.findByGwredflag", query = "SELECT j FROM Jeecriteria j WHERE j.gwredflag = :gwredflag"),
    @NamedQuery(name = "Jeecriteria.findByMessage", query = "SELECT j FROM Jeecriteria j WHERE j.message = :message"),
    @NamedQuery(name = "Jeecriteria.findByUpdatetime", query = "SELECT j FROM Jeecriteria j WHERE j.updatetime = :updatetime"),
    @NamedQuery(name = "Jeecriteria.findByLatestCriteriaNameByCell", query = "SELECT j FROM Jeecriteria j WHERE j.criteriaName = :criteriaName AND j.cellId = :cellId ORDER BY j.updatetime DESC")})
public class Jeecriteria implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "criteria_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long criteriaId;
    @Column(name = "actualresult", length = 2048)
    private String actualresult;
    @Column(name = "criteria_name", nullable=false, length = 2048)
    private String criteriaName;
    @Column(name = "expectedresult", nullable=false, length = 2048)
    private String expectedresult;
    @Column(name = "gwredflag")
    private Boolean gwredflag;
    @Lob 
    @Column(name = "message", nullable=false, length = 32768)
    private String message;
    @Column(name = "updatetime", nullable=false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatetime;
    @JoinColumn(name = "cell_id", referencedColumnName = "cell_id")
    @ManyToOne
    private Jeecell cellId;

    public Jeecriteria() {
    }

    public Jeecriteria(Long criteriaId) {
        this.criteriaId = criteriaId;
    }

    public Long getCriteriaId() {
        return criteriaId;
    }

    public void setCriteriaId(Long criteriaId) {
        this.criteriaId = criteriaId;
    }

    public String getActualresult() {
        return actualresult;
    }

    public void setActualresult(String actualresult) {
        this.actualresult = actualresult;
    }

    public String getCriteriaName() {
        return criteriaName;
    }

    public void setCriteriaName(String criteriaName) {
        this.criteriaName = criteriaName;
    }

    public String getExpectedresult() {
        return expectedresult;
    }

    public void setExpectedresult(String expectedresult) {
        this.expectedresult = expectedresult;
    }

    public Boolean getGwredflag() {
        return gwredflag;
    }

    public void setGwredflag(Boolean gwredflag) {
        this.gwredflag = gwredflag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public Jeecell getCellId() {
        return cellId;
    }

    public void setCellId(Jeecell cellId) {
        this.cellId = cellId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (criteriaId != null ? criteriaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jeecriteria)) {
            return false;
        }
        Jeecriteria other = (Jeecriteria) object;
        if ((this.criteriaId == null && other.criteriaId != null) || (this.criteriaId != null && !this.criteriaId.equals(other.criteriaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gwloader.Jeecriteria[ criteriaId=" + criteriaId + " ]";
    }
    
}
