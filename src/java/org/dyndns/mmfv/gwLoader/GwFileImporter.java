/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dyndns.mmfv.gwLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author verranm
 */
public class GwFileImporter {

    private String hostName;
    private String cellName;
    private Date dateTime;
    private Jeehost host = new Jeehost();
    private Jeecell cell = new Jeecell();
    private Properties props = new Properties();
    private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("gwLoaderPU");
    private final EntityManager em = emf.createEntityManager();

    /**
     *
     * @param propertyFile
     */
    public void importFile(File propertyFile) {

        //try {
        System.out.println("I Importing " + propertyFile.getName());

        /* 
         Convert the File to a string and crack it out into an array for
         later processing.
         */
        String temp = propertyFile.getName();
        temp = temp.substring(0, temp.lastIndexOf("."));
        String[] fileNameParts;
        fileNameParts = temp.split("_");

        /*
         Check if the filename is in the correct format. Nothing stunning, 
         just a simple sanity check. Error if its bad, or continue on to 
         break up the filename into its components to find/create objects
         later in the process.
         */
        if (fileNameParts.length != 5) {
            System.out.println("W Malformed filename - REJECTING " + propertyFile.getName());
        } else {
            System.out.println("I File appears OK");
            this.setHostName(fileNameParts[1]);
            this.setCellName(fileNameParts[2] + "_" + fileNameParts[3]);
            this.setDateTime(utils.stringToDate(fileNameParts[4]));

            try {
                FileInputStream fis;
                fis = new FileInputStream(propertyFile);
                props.load(fis);
                fis.close();
                this.setProps(props);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(GwFileImporter.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(GwFileImporter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public String getHostName() {
        return hostName;
    }

    private void setHostName(String hostName) {
        this.hostName = hostName;

        Query query = em.createNamedQuery("Jeehost.findByHostName");
        query.setParameter("hostName", this.hostName);

        if (query.getResultList().size() > 1) {
            System.out.println("DATA INTEGRITY HAS BEEN COMPROMISED. Multiple entries for host:" + this.hostName + " have been found");
            System.exit(1);
        } else if (query.getResultList().size() == 1) {
            host = (Jeehost) query.getSingleResult();
            //System.out.println("I Loaded existing host object " + host.getHostName());
        } else {
            // object can't exist, lets create it
            em.getTransaction().begin();
            //host.setHostId();
            host.setHostName(this.hostName);
            host.setHosttype("");
            em.persist(host);
            em.getTransaction().commit();
            System.out.println("I Created new host object " + host.getHostName());

        }

    }

    public String getCellName() {
        return cellName;
    }

    private void setCellName(String cellName) {
        this.cellName = cellName;

        Query query = em.createNamedQuery("Jeecell.findByCellNameAndHostId");
        query.setParameter("cellName", this.cellName);
        query.setParameter("hostId", host);

        if (query.getResultList().size() > 1) {
            System.out.println("E DATA INTEGRITY HAS BEEN COMPROMISED. Multiple entries for cell:" + this.cellName + "/" + this.hostName + " have been found");
            System.exit(1);
        } else if (query.getResultList().size() == 1) {
            cell = (Jeecell) query.getSingleResult();
            //System.out.println("I Loaded existing cell object " + cell.getCellName() + "/" + host.getHostName());
        } else {
            // object can't exist, lets create it
            em.getTransaction().begin();
            cell.setCellName(cellName);
            cell.setHostId(host);
            em.persist(cell);
            em.getTransaction().commit();
            System.out.println("I Created new cell object " + cell.getCellName() + "/" + host.getHostName());

        }

    }

    public Date getDateTime() {
        return dateTime;
    }

    private void setDateTime(Date dateTime) {
        if (dateTime == null) {
            System.out.println("Malformed date!");
        }
        this.dateTime = dateTime;
    }

    public Properties getProps() {
        return props;
    }

    private void setProps(Properties props) {
        this.props = props;

        // Get a list of the the test criterias as a unique list
        // ie trim the last part of the property name.
        ArrayList<String> propNames = new ArrayList<String>();
        for (Object propKey : props.keySet()) {
            String temp = propKey.toString();
            String criteriaName;
            if (temp.contains(".")) {
                criteriaName=temp.substring(0, temp.lastIndexOf("."));
                if (propNames.indexOf(criteriaName) == -1) {
                    propNames.add(criteriaName);
                }
            } else {
                System.out.println("W Rejected property " + temp + " as it is not . separated.");
            }
        }

        for (String eachProp : propNames) {
            em.getTransaction().begin();

            // Create an iterim object with our cell id...
            Jeecriteria tempCriteria = new Jeecriteria();
            tempCriteria.setCellId(cell);
            tempCriteria.setUpdatetime(dateTime);
            tempCriteria.setCriteriaName(eachProp);
            tempCriteria.setExpectedresult(props.getProperty(eachProp + ".expected", ""));
            tempCriteria.setActualresult(props.getProperty(eachProp + ".actual", ""));
            tempCriteria.setMessage(props.getProperty(eachProp + ".message", ""));
            tempCriteria.setGwredflag(Boolean.valueOf(props.getProperty(eachProp + ".gwredflag", "")));

            // get the latest props record for this host/cell/eachProp and check if its different
            Query query = em.createNamedQuery("Jeecriteria.findByLatestCriteriaNameByCell");
            query.setParameter("criteriaName", eachProp);
            query.setParameter("cellId", cell);

            List results = query.getResultList();
            if (results.size() > 0) {
                // we have records
                // lets check the date on the latest... (query is in desc date order)

                Jeecriteria savedCriteria = (Jeecriteria) results.get(0); // we only want the first
                if (tempCriteria.getUpdatetime().after(savedCriteria.getUpdatetime())) {
                    // we have a potential update, compare the fields...
                    if (!tempCriteria.getActualresult().equals(savedCriteria.getActualresult())
                            | !tempCriteria.getExpectedresult().equals(savedCriteria.getExpectedresult())
                            | !tempCriteria.getMessage().equals(savedCriteria.getMessage())
                            | !tempCriteria.getGwredflag().equals(savedCriteria.getGwredflag())) {
                        // we have an update.
                        System.out.println("I Adding newer property:" + eachProp);
                        em.persist(tempCriteria);
                    }
                } else {
                    // the record is older than data we already have
                    System.out.println("W Property in file for " + eachProp + " is older than latest in database, ignoring.");
                }

            } else {
                // we have no existing records, so lets simply add.
                System.out.println("I Creating new property: " + eachProp);
                em.persist(tempCriteria);
            }
            em.getTransaction().commit();
        }

    }

}
