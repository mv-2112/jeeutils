package org.dyndns.mmfv;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.net.InetAddressCachePolicy;

public class DNSTester {
    InetAddress[] lookupAddress;
    
    public InetAddress[] lookup(String hostToLookup) {
        if (hostToLookup != null) {
            try {
                this.lookupAddress = InetAddress.getAllByName(hostToLookup);
                for (InetAddress eachAddress : this.lookupAddress)
                    System.out.println("INFO  ResolvableAddress "+eachAddress);
            } catch (UnknownHostException ex) {
                System.out.println("ERROR lookup of " + hostToLookup + " failed!");
                Logger.getLogger(DNSTester.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("Cache policy: " + InetAddressCachePolicy.get());
        return this.lookupAddress;
    }
}