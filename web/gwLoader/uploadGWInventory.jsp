<%-- 
    Document   : index
    Created on : Feb 18, 2014, 9:25:32 AM
    Author     : verranm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>GWInventory file uploader</title>
    </head>
    <body>
        <h1>GWInventory file uploader</h1>
        <p>Use this site to upload GWInventory output from the WebSphere estate.</p>
        <form name="uploadGWInvFile" action="result.jsp" enctype="multipart/form-data" method="post">
            <input type="file" name="fileName" size="65536"/>
            <input type="submit" value="upload" name="action" />
        </form>
    </body>
</html>


