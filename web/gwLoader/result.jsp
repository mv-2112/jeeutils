


<jsp:useBean id="gwLoader" scope="page" class="org.dyndns.mmfv.gwLoader.GwFileImporter" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
    String action = request.getParameter("action");
    String message = "";

    if (action.equals("upload")) {
        String filePath = request.getParameter(fileName);
        message = "";
    } else {
        action = "unknown";
        message = "";
    }
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= action%></title>
    </head>
    <body>
        <h1><%= action%></h1>
        <%= message%>
    </body>
</html>
