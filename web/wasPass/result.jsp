<%-- 
    Document   : result
    Created on : Feb 18, 2014, 10:20:27 AM
    Author     : verranm
--%>

<jsp:useBean id="password" scope="page" class="org.dyndns.mmfv.xorPass" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
    String key = "_";
    String outPassword = "";
    String action = request.getParameter("action");
    String message = "";

    if (action.equals("encode")) {
        String encodePassword = request.getParameter("encodePassword");
        outPassword = password.encode(encodePassword, key);
        message = "Password [" + encodePassword + "] after encoding is [{xor}" + outPassword + "].";
    } else if (action.equals("decode")) {
        String decodePassword = request.getParameter("decodePassword");
        if ( decodePassword.startsWith("{xor}") ) {
            decodePassword = decodePassword.substring(5);
        }
        outPassword = password.decode(decodePassword, key);
        message = "Password [{xor}" + decodePassword + "] after decoding is [" + outPassword + "].";
    } else {
        action = "unknown";
        message = "You have requested an unsupported or unimplemented feature.";
    }
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= action%></title>
    </head>
    <body>
        <h1><%= action%></h1>
        <%= message%>
    </body>
</html>
