<%-- 
    Document   : index
    Created on : Feb 18, 2014, 9:25:32 AM
    Author     : verranm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>WAS Password encode/decode</title>
    </head>
    <body>
        <h1>WAS Password assistant</h1>
        <p>Use this site to pre-encrypt and check passwords for use in the WebSphere estate.</p>
        <form name="encodePassword" action="result.jsp">
            <input type="text" name="encodePassword"/>
            <input type="submit" value="encode" name="action" />
        </form>
        <form name="decodePassword" action="result.jsp">       
            <input type="text" name="decodePassword"/>
            <input type="submit" value="decode" name="action" />
        </form>
    </body>
</html>
