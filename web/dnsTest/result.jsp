
<%@page import="java.net.InetAddress"%>
<jsp:useBean id="dnsTester" scope="page" class="org.dyndns.mmfv.DNSTester" />
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%

    String hostToLookup;
    String action = request.getParameter("action");
    InetAddress addressesFound[];
    String message;

    if (action.equals("lookup")) {
        hostToLookup = request.getParameter("hostToLookup");
        addressesFound = dnsTester.lookup(hostToLookup);
        message = "DNS host to lookup: ";
        message += hostToLookup;
    } else {
        action = "unknown";
        message = "You have requested an unsupported or unimplemented feature.";
        addressesFound = null;
    }

%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= action%></title>
    </head>
    <body>
        <h1><%= action%></h1>
        <%= message%>
        <ul>
            <%
                try {
                    for (InetAddress eachAddress : addressesFound) {
            %>
            <li>
                <%= eachAddress%>
            </li>
            <%
                }
            } catch (NullPointerException npe) {
            %>
            <li>
                No addresses found
            </li>
            <%
                }
            %>
        </ul>
        <p>Cache Policy: <%= sun.net.InetAddressCachePolicy.get() %></p>
    </body>
</html>