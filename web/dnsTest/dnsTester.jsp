<%-- 
    Document   : index
    Created on : Feb 18, 2014, 9:25:32 AM
    Author     : verranm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DNS/TTL tester</title>
    </head>
    <body>
        <h1>DNS/TTL tester</h1>
        <p>Use this page to query the state of DNS and TTL configuration.</p>
        <form name="dnsTester" action="result.jsp">
            <input type="text" name="hostToLookup"/>
            <input type="submit" value="lookup" name="action" />
        </form>
    </body>
</html>
